#encrypter.py
import sys
import base64
import hashlib
import math
from Crypto.Cipher import AES

# Parametros
# DELIMITER : Delimitador del archivo
# PASSWORD : String a usar en la llave
# COLS_2_ENCRYPT : Columnas a encriptar

# Uso: cat file.csv|python encrypter.py >> new_file_encrypted.csv

DELIMITER='|'
PASSWORD="abc"
COLS_2_ENCRYPT = [1,3]

cipher = AES.new(PASSWORD.rjust(16),AES.MODE_ECB)


for line in sys.stdin:
	line_array = line.split(DELIMITER)
	for i in COLS_2_ENCRYPT:
		# Columnas a encriptar siempre deben ser multiplos de 16 bytes
		fixed_token=line_array[i].rjust(math.ceil(len(line_array[i])/16)*16)
		encoded = base64.b64encode(cipher.encrypt(fixed_token))
		line_array[i]=encoded.decode("utf-8")	
	new_line=DELIMITER.join(str(x) for x in line_array)
	sys.stdout.write(new_line)
sys.stdout.flush()
