# landing2archive.sh by Karim [karim at touma.io]

# This script get a landing folder and reorder that files to date structures.
# It takes dinamically the date from the file assuming YYYY-MM-DD date pattern, this is a MUST
# Using directly data from Google Cloud Storage, so you need the gsutil sdk installed.
# https://cloud.google.com/storage/docs/gsutil_install
# Just api call ;]

# Usage:

# sh landing2archive.sh -f FILE -a ARCHIVE_FOLDER

# Regular expression for gs://
REGS='^gs(:\/\/)*'
# Regular expression for date
DREG='[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}'

if gsutil --version ; then
	while getopts ":f:a:" opt; do
	  case $opt in
	    f)		
			# Validacion de ruta gs://
			if [[ $OPTARG =~ $REGS ]]; then
				if gsutil -q stat $OPTARG ; then
					echo "[LOG] File `basename $OPTARG` found in Google Cloud Storage"
					# Lot of variables to play with
					FILE_PATH_TO_PROCESS=$OPTARG
					FILE_BASENAME_TO_PROCESS=`basename $OPTARG`
					DATE_FILE=`echo $FILE_BASENAME_TO_PROCESS|grep -Eo $DREG`
					# grep got input as echo stdin or you can also use cat command if you have these strings in a file.
					# -E Interpret PATTERN as an extended regular expression.
					# -o Show only the part of a matching line that matches PATTERN.
					# [[:digit:]] It will fetch digit only from input.
					# {N} It will check N number of digits in given string, i.e.: 4 for years 2 for months and days
					if [ ${#DATE_FILE} == 10 ]; then 
						echo "[LOG] Date infile is OK"
  					  	YYYY=${DATE_FILE:0:4}
						MM=${DATE_FILE:5:2}
						DD=${DATE_FILE:8:2}
    				else 
    					echo "[ERROR] Date wrong from file: $DATE_FILE"
						exit 1
					fi
					echo "[LOG] Saved to process: $FILE_TO_PROCESS"
					echo "[LOG] File name: $FILE_BASENAME_TO_PROCESS"
					echo "[LOG] Date identified: $DATE_FILE"
				else
					echo "[ERROR] File: `basename $OPTARG`"
					echo "[ERROR] NOT FOUND in Google Cloud Storage"
					exit 1
				fi
		  	else
		  		echo "[ERROR] File path is not a gs:// protocol" 
		  		exit 1 
			fi
	      ;;
	    a)
			# Validacion de ruta gs://
			if [[ $OPTARG =~ $REGS ]]; then
				# testing operations in the archive
				# Triying to emulate a dryrun:
				# Supressing CommandException to check if object exists
				ARCHIVE_PATH=`gsutil ls -d $OPTARG 2> /dev/null`
				# If last script works it will 
				if [[ $ARCHIVE_PATH =~ $REGS ]]; then
					echo "[LOG] Archive set: $ARCHIVE_PATH"
				else 
					echo "[ERROR] Unable to access archive: $OPTARG"
					exit 1
				fi
		  	else
		  		echo "[ERROR] Archive path is not a gs:// protocol" 
		  		exit 1 
			fi
	      ;;
	    \?)
	      echo "[ERROR] Invalid option: -$OPTARG" >&2
	      exit 1
	      ;;
	    :)
	      echo "[ERROR] Option `basename $0` -$OPTARG requires arguments" >&2
	      exit 1
	      ;;
	  esac
	done

	OUTPUT_FOLDER="$ARCHIVE_PATH$YYYY/$MM/$DD/"
	echo "[LOG] All ok to move file to archive"
	if ! gsutil cp $FILE_PATH_TO_PROCESS $OUTPUT_FOLDER; then
  		echo "[ERROR] Failed to copy, beware of deleting:"
  		echo "[ERROR] $FILE_PATH_TO_PROCESS"
  		echo "[ERROR] Ending"
  		exit 1
	fi

	if ! gsutil rm $FILE_PATH_TO_PROCESS; then
  		echo "[ERROR] This is weird, failed to delete"
  		echo "[ERROR] $FILE_PATH_TO_PROCESS"
  		echo "[ERROR] Ending"
  		exit 1
	fi

	echo "[LOG] File succesfully moved from:"
	echo "[LOG] $FILE_PATH_TO_PROCESS"
	echo "[LOG] to"
	echo "[LOG] $OUTPUT_FOLDER"
	exit 0
else
    echo "[ERROR] Google SDK gsutil is required: https://cloud.google.com/storage/docs/gsutil_install"
    exit 1
fi
