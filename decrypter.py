#decrypter.py

#encrypter.py
import sys
import base64
import hashlib
import math
from Crypto.Cipher import AES

# Parametros
# DELIMITER : Delimitador del archivo
# PASSWORD : String a usar en la llave
# COLS_2_DECRYPT : Columnas a desencriptar

# Uso: cat new_file_encrypted.csv|python decrypter.py >> file.csv

DELIMITER='|'
PASSWORD="abc"
COLS_2_DECRYPT = [1,3]

cipher = AES.new(PASSWORD.rjust(16),AES.MODE_ECB)


for line in sys.stdin:
	line_array = line.split(DELIMITER)
	for i in COLS_2_DECRYPT:
		# Columnas encryptadas siempre deben ser multiplos de 16 bytes
		fixed_token=line_array[i].rjust(math.ceil(len(line_array[i])/16)*16)
		decoded = cipher.decrypt(base64.b64decode(fixed_token))
		line_array[i]=decoded.decode("utf-8").strip()
	new_line=DELIMITER.join(str(x) for x in line_array)
	sys.stdout.write(new_line)
sys.stdout.flush()
